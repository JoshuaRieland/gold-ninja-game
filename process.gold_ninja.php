<?php

SESSION_START();

if(isset($_POST['action']) && $_POST['action'] != 'Casino')
{	
	switch($_POST['action'])
	{
		case 'Farm':
		$_gold = rand(10,20);
		$_SESSION['currentGold'] += $_gold;
		$_SESSION['msg'] = "<p>You entered a Farm and earned $_gold</p>" . $_SESSION['msg'];	
		header("Location: index.php");
		break;

		case 'Cave':
		$_gold = rand(5,10);
		$_SESSION['currentGold'] += $_gold;
		$_SESSION['msg'] = "<p>You entered a Cave and earned $_gold</p>" . $_SESSION['msg'];			
		header("Location: index.php");
		break;

		case 'House':
		$_gold = rand(2,5);
		$_SESSION['currentGold'] += $_gold;
		$_SESSION['msg'] = "<p>You entered a House and earned $_gold</p>" . $_SESSION['msg'];	
		header("Location: index.php");
		break;
	}

}
else if(isset($_POST['action']) && $_POST['action'] == 'Casino')
{
	$_gold = rand(-50,50);
	if($_SESSION['currentGold'] == 0)
	{
		$_SESSION['msg'] = "<p>Not A Single Gold Scrap To Gamble! Go To Work!!!</p>" . $_SESSION['msg'];	
		header("Location: index.php");
	}
	else if($_SESSION['currentGold'] < 0)
	{
		$_SESSION['msg'] = "<p>You Owe The Casino $$. Watch Out, Rough Waters Ahead!</p>" . $_SESSION['msg'];	
		header("Location: index.php");		
	}
	else
	{
		if($_gold < 0)
		{
			$_SESSION['currentGold'] += $_gold;
			$_SESSION['msg'] = "<p class='red'>You LOST $_gold gold! Bad Luck!</p>" . $_SESSION['msg'];
		}
		else if($_gold == 0)
		{
			$_SESSION['msg'] = "<p class='blue'>You gambled and came out even! Not bad, not good!</p>" . $_SESSION['msg'];
		}
		else if($_gold > 0)
		{
			$_SESSION['currentGold'] += $_gold;
			$_SESSION['msg'] = "<p>You WON $_gold gold! Luck of the Irish is with you!</p>" . $_SESSION['msg'];
		}
	header("Location: index.php");
	}
}

if(isset($_POST['reset']))
{

	$_SESSION['currentGold'] = 0;
	$_SESSION['msg'] = "";
	header("Location: index.php");
}
?>