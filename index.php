<?php 
	SESSION_START();

	if(!isset($_SESSION['currentGold']))
	{
		$_SESSION['currentGold'] = 0;
	}
?>

<html>
<head>
	<title>Ninja Gold</title>
	<link rel="stylesheet" type="text/css" href="style.gold_ninja.css">
</head>
<body>
		
	<div class="row">
		<h1>Your Gold: <?= $_SESSION['currentGold'] ?> </h1>
	</div>
	
	<div class="row">
		<div class="box boxFarm">
			<h3>Farm</h3>
			<h5>(Earn 10-20 Gold)</h5>
			<form method="post" action="process.gold_ninja.php">
				<input type="hidden" name="action" value="Farm">
				<input type="submit" value='Find Gold!'>
			</form>
		</div>

		<div class="box boxCave">
			<h3>Cave</h3>
			<h5>(Earn 5-10 Gold)</h5>
			<form method="post" action="process.gold_ninja.php">
				<input type="hidden" name="action" value="Cave">
				<input type="submit" value='Find Gold!'>
			</form>
		</div>

		<div class="box boxHouse">
			<h3>House</h3>
			<h5>(Earn 2-5 Gold)</h5>
			<form method="post" action="process.gold_ninja.php">
				<input type="hidden" name="action" value="House">
				<input type="submit" value='Find Gold!'>
			</form>
		</div>

		<div class="box boxCasino">
			<h3>Casino</h3>
			<h5>(Earn 0-50 Gold)</h3>
			<form method="post" action="process.gold_ninja.php">
				<input type="hidden" name="action" value="Casino">
				<input type="submit" value='Find Gold!'>
			</form>
	</div>
	
	<div class="row msgBox">
		<?php 
			echo $_SESSION['msg'];
		?>
	</div>
	<form method="post" action="process.gold_ninja.php">
		<input type="hidden" name="reset" value="reset">
		<input type="submit" value="RESET GOLD!" class="reset">
	
</body>
</html>